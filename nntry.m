%initialize parameter
[itera,iterb] = size(theta);

n = 100;
neuron =5;
layer = 4;
lamda= 0.5;
moment=0.3;

W = [1 0.7 0.6 0.5 0.5; 0.5 0.5 0.4 0.4 0.3; 0.2 0.1 1.0 0.1.0 1.0; 0.3 0.3 0.2 0.1 0.4; 0.5 0.5 0.5 0.5 0.5];
W(:,:,2) = [0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5];
W(:,:,3) = [0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5];
W(:,:,4) = [0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5; 0.5 0.5 0.5 0.5 0.5];

delta = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];
delta(:,:,2) = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];
delta(:,:,3) = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];
delta(:,:,4) = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];

delw1=0;delw2=0;delw3=0;
delb1=0;delb2=0;delb3=0;

B = [0.5 ; 0.5; 0.5 ; 0.5 ; 0.5];
B(:,:,2) = [0.5 ; 0.5; 0.5 ; 0.5 ; 0.5];
B(:,:,3) = [0.5 ; 0.5; 0.5 ; 0.5 ; 0.5];
B(:,:,4) = [0.5 ; 0.5; 0.5 ; 0.5 ; 0.5];
B(:,:,5) = [0.5 ; 0.5; 0.5 ; 0.5 ; 0.5];

X = [0 ; 0; 0 ; 0 ; 0];
X(:,:,2) = [0 ; 0; 0 ; 0 ; 0];
X(:,:,3) = [0 ; 0; 0 ; 0 ; 0];
X(:,:,4) = [0 ; 0; 0 ; 0 ; 0];

y = [0 ; 0; 0 ; 0 ; 0];
y(:,:,2) = [0 ; 0; 0 ; 0 ; 0];
y(:,:,3) = [0 ; 0; 0 ; 0 ; 0];
y(:,:,4) = [0 ; 0; 0 ; 0 ; 0];

thetaEx=ones(itera-1,1);
dthetaEx=ones(itera-1,1);
ddthetaEx=ones(itera-1,1);

errortheta=ones(itera-1,1);
errordtheta=ones(itera-1,1);
errorddtheta=ones(itera-1,1);


%total iteration
for ii=1:itera-1
%forward NN
for k=1:layer
    for j=1:neuron
        if (k==1)
            X(j,1,k)=W(j,1,k)*theta(ii,1) + W(j,2,k)*dtheta(ii,1) + W(j,3,k)*ddtheta(ii,1) +W(j,4,k)*inDp(ii,1) +W(j,5,k)*inBk(ii,1)+ B(j,1,k);
        elseif (k==2)
            neuron=3;
            X(j,1,k)=W(j,1,k)*Y(1,1,k-1) + W(j,2,k)*Y(2,1,k-1) + W(j,3,k)*Y(3,1,k-1) +W(j,4,k)*Y(4,1,k-1) +W(j,5,k)*Y(5,1,k-1)+B(j,1,k);
        elseif (k>=3)
            neuron=3;
            X(j,1,k)=W(j,1,k)*Y(1,1,k-1) + W(j,2,k)*Y(2,1,k-1) + W(j,3,k)*Y(3,1,k-1) + B(j,1,k);
        end
        Y(j,1,k)=1/(1+exp(-X(j,1,k)));
        
        if (k<=4)
            X(j,1,k+1)=Y(j,1,k);
        end
     end
end

%save forward propagation
thetaEx(ii,1)=Y(1,1,layer);
dthetaEx(ii,1)=Y(2,1,layer);
ddthetaEx(ii,1)=Y(3,1,layer);

errortheta(ii,1)=thetaEx(ii,1)-theta(ii+1,1);
errordtheta(ii,1)=dthetaEx(ii,1)-dtheta(ii+1,1);
errorddtheta(ii,1)=ddthetaEx(ii,1)-ddtheta(ii+1,1);


%backpropagation
for k=layer:-1:1
//ii    
   for j=1:neuron
       if (k==4)
           delta(j,1,k)=Y(1,1,k)*(1-Y(1,1,k))*(Y(1,1,k)-theta(ii+1,1));
           delta(j,2,k)=Y(2,1,k)*(1-Y(2,1,k))*(Y(2,1,k)-dtheta(ii+1,1));
           delta(j,3,k)=Y(3,1,k)*(1-Y(3,1,k))*(Y(3,1,k)-ddtheta(ii+1,1));
           
           delw1=-lamda*delta(j,1,k)*Y(1,1,k-1);
           delw2=-lamda*delta(j,2,k)*Y(2,1,k-1);
           delw3=-lamda*delta(j,3,k)*Y(3,1,k-1);
           
           delb1=lamda*delta(1,1,k);
           delb2=lamda*delta(1,2,k);
           delb3=lamda*delta(1,3,k);
           
           W(j,1,k)= moment*W(j,1,k) + (1-moment)*(W(j,1,k)+delw1);
           W(j,2,k)= moment*W(j,2,k) + (1-moment)*(W(j,2,k)+delw2);
           W(j,3,k)= moment*W(j,3,k) + (1-moment)*(W(j,3,k)+delw3);
           
           B(1,1,k)= moment*B(1,1,k) + (1-moment)*(B(1,1,k)+delb1);
           B(2,1,k)= moment*B(2,1,k) + (1-moment)*(B(2,1,k)+delb2);
           B(3,1,k)= moment*B(3,1,k) + (1-moment)*(B(3,1,k)+delb3);
           
           j=neuron;
       elseif ((k<4)&&(k>2))
           for i=1:neuron
               delta(j,i,k)=Y(j,1,k)*(1-Y(j,1,k))*(delta(i,1,k+1)*W(i,1,k+1)+delta(i,2,k+1)*W(i,2,k+1)+delta(i,3,k+1)*W(i,3,k+1));
               
               delw1=-lamda*delta(j,i,k)*Y(j,1,k-1);
               W(j,i,k)= moment*W(j,i,k) + (1-moment)*(W(j,i,k)+delw1);
           end
           
               delb1=lamda*delta(1,j,k);
                            
               B(j,1,k)= moment*B(j,1,k) + (1-moment)*(B(j,1,k)+delb1);
               
           
       elseif (k==2)
           neuron=5;
           for i=1:3
               delta(j,i,k)=Y(i,1,k)*(1-Y(i,1,k))*(delta(i,1,k+1)*W(i,1,k+1)+delta(i,2,k+1)*W(i,2,k+1)+delta(i,3,k+1)*W(i,3,k+1));
               
               delw1=-lamda*delta(j,i,k)*Y(j,1,k-1);
               W(j,i,k)= moment*W(j,i,k) + (1-moment)*(W(j,i,k)+delw1);
           end
               delb1=lamda*delta(1,j,k);
                           
               B(j,1,k)= moment*B(j,1,k) + (1-moment)*(B(j,1,k)+delb1);
           
       elseif (k==1)
           for i=1:neuron
                delta(j,i,k)=Y(i,1,k)*(1-Y(i,1,k))*(delta(i,1,k+1)*W(i,1,k+1)+delta(i,2,k+1)*W(i,2,k+1)+delta(i,3,k+1)*W(i,3,k+1)+delta(i,4,k+1)*W(i,4,k+1)+delta(i,5,k+1)*W(i,5,k+1));
                
                if (i==1)
                    delw1=-lamda*delta(j,i,k)*theta(ii,1);
                elseif (i==2)
                    delw1=-lamda*delta(j,i,k)*dtheta(ii,1);
                elseif (i==3)
                    delw1=-lamda*delta(j,i,k)*ddtheta(ii,1);
                elseif (i==4)
                    delw1=-lamda*delta(j,i,k)*inDp(ii,1);
                elseif (i==5)
                    delw1=-lamda*delta(j,i,k)*inBk(ii,1);
                end
            W(j,i,k)= moment*W(j,i,k) + (1-moment)*(W(j,i,k)+delw1);
            delb1=lamda*delta(j,1,k);
               
            B(j,1,k)= moment*B(j,1,k) + (1-moment)*(B(j,1,k)+delb1);
           
           end
           
           
       end
       
       
   end
   
end
end
