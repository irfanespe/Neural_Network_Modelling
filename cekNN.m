%initialize parameter
[itera,iterb] = size(input);

n = 100;
neuron =3;
layer = 3;
lamda= 0.4;
moment=0.6;

W = [0.1 0.2 0.45; 0.55 0.2 0.1; 0.6 0.2 0.35];
W(:,:,2) = [0.2 1.2 0.15; 0.95 0.8 0.1; 0.4 0.1 0.75];
W(:,:,3) = [1.3 0.9 0.75; 0.85 0.2 0.1; 0.6 0.2 0.35];

delta = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];
delta(:,:,2) = [0 0 0 0 0; 0 0 0 0 0; 0 0 0 0 0;0 0 0 0 0; 0 0 0 0 0];

delw1=0;delw2=0;delw3=0;
delb1=0;delb2=0;delb3=0;

B = [1 ; 0.1; 0.3];
B(:,:,2) = [0.2; 0.1; 2.1 ];
B(:,:,3) = [0.12; 0.2; 9.8 ];

X = [0 ; 0; 0 ; 0 ; 0];
X(:,:,2) = [0 ; 0; 0 ; 0 ; 0];
X(:,:,3) = [0 ; 0; 0 ; 0 ; 0];

Y = [0 ; 0; 0 ; 0 ; 0];
Y(:,:,2) = [0 ; 0; 0 ; 0 ; 0];
Y(:,:,3) = [0 ; 0; 0 ; 0 ; 0];



yEx=ones(itera-1,1);

errory=ones(itera-1,1);

deltaw=ones(3,3,3,itera);

%total iteration
for ii=2:itera
%for ii=2:57
%forward NN
%ii=2;

for k=1:layer
    for j=1:neuron
        if (k==1)
            X(j,1,k)=W(1,j,k)*input(ii,1) + W(2,j,k)*out(ii-1,1)+ W(3,j,k)*t(ii,1)+B(j,1,k);
        elseif (k==2)
            
            X(j,1,k)=W(1,j,k)*Y(1,1,k-1) + W(2,j,k)*Y(2,1,k-1) + W(3,j,k)*Y(3,1,k-1)+B(j,1,k);
            
        elseif (k==3)
            X(j,1,k)=W(1,j,k)*Y(1,1,k-1) + W(2,j,k)*Y(2,1,k-1) + W(3,j,k)*Y(3,1,k-1)+B(j,1,k);
            
        end
        
            %Y(j,1,k)=1/(1+exp(-X(j,1,k)));
        if (k==layer)
            Y(j,1,k)=X(j,1,k);
        else
            Y(j,1,k)=1/(1+exp(-X(j,1,k)));
        end
        
        if(k==layer)
                j=3;
        end
        end
end

%save forward propagation
yEx(ii,1)=Y(1,1,3);

errory(ii,1)=yEx(ii,1)-out(ii,1);

%backpropagation
for k=layer:-1:1    
   for j=1:neuron
       if (k==layer)
           delta(j,1,k)=(Y(1,1,k)-out(ii,1));
          
           delw1=-1*lamda*delta(j,1,k)*Y(j,1,k-1);
           
           deltaw(ii,1)=delw1;
           
           deltaw(j,1,k,itera)=delw1;
           
           W(j,1,k)= moment*W(j,1,k) + (1-moment)*(W(j,1,k)+delw1);
          
           if (j==3) 
               delb1=lamda*delta(1,1,k); 
               B(1,1,k)= moment*B(1,1,k) + (1-moment)*(B(1,1,k)+delb1);
           end
           
       elseif (k==2)
          for i=1:neuron
            delta(j,i,k)=Y(j,1,k)*(1-Y(j,1,k))*(delta(i,1,k+1)*W(i,1,k+1));
            delw1=-1*lamda*delta(j,i,k)*Y(j,1,k-1);
           
            deltaw(j,i,k,itera)=delw1;
            
            W(j,i,k)= moment*W(j,i,k) + (1-moment)*(W(j,i,k)+delw1);
          
            if (j==3)
                delb1=lamda*delta(i,1,k);
                B(i,1,k)= moment*B(i,1,k) + (1-moment)*(B(1,1,k)+delb1);
            end
          end
       elseif (k==1)    
           for i=1:neuron
               delta(j,i,k)=Y(j,1,k)*(1-Y(j,1,k))*(delta(i,1,k+1)*W(i,1,k+1)+delta(i,2,k+1)*W(i,2,k+1)+delta(i,3,k+1)*W(i,3,k+1));
               
               if (j==1)
                    delw1=-1*lamda*delta(j,i,k)*input(ii);
               elseif(j==2)
                    delw1=-1*lamda*delta(j,i,k)*out(ii-1);
               elseif(j==3)
                    delw1=-1*lamda*delta(j,i,k)*t(ii);
               end
               
                    deltaw(j,1,k,itera)=delw1;
                    
                    W(j,i,k)= moment*W(j,i,k) + (1-moment)*(W(j,i,k)+delw1);
               if (j==3)
                    delb1=lamda*delta(j,i,k);
                            
                    B(j,1,k)= moment*B(j,1,k) + (1-moment)*(B(j,1,k)+delb1);
               end
           end
           
              
               
           
       end
       
       
   end 
end
end